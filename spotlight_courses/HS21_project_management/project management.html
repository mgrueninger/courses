<!DOCTYPE html>
<html>
  <head>
    <title>IT Project Management</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="css/remark.css">
    <link rel="stylesheet" media="print" href="css/remark.print.css">
  </head>
  <body>
    <textarea id="source">

class: center, middle

# IT project management

<img src="images/thealternative-logo.jpg" width="30%">

---

# Motivation

IT projects fail often (2017):
- 14% fail completely
- 31% didn’t meet their goals
- 43% exceeded initial budgets
- 49% were late
- 69% succeed

<small>
[Source](http://www.pmi.org/-/media/pmi/documents/public/pdf/learning/thought-leadership/pulse/pulse-of-the-profession-2017.pdf?sc_lang_temp=en)
</small>

<small>
*Sometimes success rates as low 16% are claimed, referencing a widely cited [report of 1994](https://www.standishgroup.com/sample_research_files/chaos_report_1994.pdf).*
</small>

---

# Why projects fail

Nearly universally referenced issues:
- Inaccurate requirements
- Uninvolved project sponsors
- Inaccurate estimates
- Unexpected risks

[Source 1](https://www.atspoke.com/blog/it/reasons-for-it-project-failure/), [Source 2](https://www.orellfuessli.ch/shop/home/artikeldetails/ID39325699.html), [Source 3](https://www.gartner.com/en/documents/2892218), PMO Survey 2020

<img src="images/project-fail-reasons.png" width="100%">



---

# Empirical data

Unfortunately, mostly useless.

Littered with strong opinions, baseless claims, bad statistics, unprocessed experience, ...

<img src="images/success_rate_vs_project_cost.png" width="100%">

---

# Why projects fail

List out of "Project Recovery" by Harold Kerzner

- End-user stakeholders not involved throughout the project
- Minimal or no stakeholder backing; lack of ownership
- Weak initial business case
- Business case deterioration
- Business case requirements that changed significantly over the life of the project
- Technical obsolescence
- Technologically unrealistic requirements
- Lack of a clear vision
- New executive team in place with different visions and goals
- Corporate goals and/or vision not understood at the lower organizational level
- Plan that asks for too much in too little time
- Poor estimates, especially financial
- Unclear stakeholder requirements
- Passive user stakeholder involvement after handoff
- Unclear or unrealistic expectations
- Unrealistic assumptions, if they exist at all
- Plans based upon insufficient data
- No systemization of the planning process
- Planning performed by a planning group
- Inadequate or incomplete requirements
- Lack of resources
- Assigned resources that lack experience or the necessary skills

---

- Resources that lack focus or motivation
- Staffing requirements that are not fully known
- Constantly changing resources
- Poor overall project planning
- Established milestones not measurable
- Established milestones too far apart
- Environmental factors that have changes causing outdated scope
- Missed deadlines and no recovery plan
- Budgets exceeded and out of control
- Lack of replanning on a regular basis
- Lack of attention provided to human and organizational aspects of project
- Project estimates that are best guesses and not based upon history or standards
- Not enough time provided for estimating
- Exact major milestone dates or due dates for reporting not known
- Team members working with conflicting requirements
- People shuffled in and out of project with little regard for schedule
- Poor or fragmented cost control
- Each stakeholder uses different organizational process assets, which may be incompatible with each other
- Weak project and stakeholder communications
- Poor assessment of risks, if done at all
- Wrong type of contract
- Poor project management; team members possess a poor understanding of project management, especially virtual team members
- Technical objectives that are more important than business objectives
- Assigning critically skilled workers, including the project manager, on a part-time basis

---

# Applicability of course

IT-projects as a freelancer.

Yes:
- Marketing webpage for the business of your dad
- Online shop for small printing shop
- eVoting application for members of UZH
- App & Webapplication combination for construction firm

No:
- Führungsinformationssystem Heer
- Mainframe migration
- Medical software 
- Bitcoin trading bot

---

# Experience

Until bachelors:
- Freelancer webpages since 2014
- Bachelor ETH (Computer Science) 2015 - 2018
- Projectmanagement & Development @ JKweb 2016 - 2017
- Freelancer webapplications since 2018

After bachelors:
- Professional Software Engineer @ Zühlke 2018 - 2019
- VSETH board member 2019-2020
- Master ETH (Computer Science) 2019 - 2022

Projects managed:
- 12 projects @ JKweb
- 16 projects as Freelancer (of which 10 were bigger applications)
- ~80 projects in total

Projects: https://famoser.ch/

---

# This course

Parts:
- **Know what makes a good project**: High-level view about a suitable environment for your project.
- **Plan a successful project**: How to create a convincing project plan.
- **Execute a successful project**: How to execute & finish your project.

Targets:
- You can detect favorable environments for projects
- You feel confident creating a realistic project schedule
- You can finish a project
	- within budget
	- within schedule
	- which satisfies customer expectations.

---

class: center, middle

# Preconditions for a successful project

<i>Claim: Small projects (&lt;300h) succeed or fail before the first line of code is written.</i>

---

# Project Vision

The *elevator pitch* of your project.

A good vision:
- Is short
- Focuses on the resulting benefits
- Motivates

For bigger projects:
- Might include sub-visions 
- Might include non-visions

> The tool allows managers of a construction site to easily create and manage issues, and communicate those to the responsible craftsmen.

---

# Business environment

business case:
- alternatives? cost-benefit?
- sensible?
- stable during the project?

> Tinder for jobs.

stakeholder(s):
- agree about scope & purpose of project?
- have realistic expectations?
- personally support with time and money?

> E-Learning Platform.

---

# You

*sufficient experience*. Not too many unknowns, experience with most parts of final product.

> Issue-management requires a web (frontend + backend) / app combination. Server-generated content, CRUD, API, heavy client-side functionality, usability, many features, large data storage, synchronization, translations.

*sufficient trust*. Understand where trust is coming from and how to maintain it.

> Based on a previous project: Deliver early.  
> Based on personal recommendation: Maintain relationships.

*sufficient capabilities*. Control over technological & organisational aspects.

> Need to deploy to a server: Have full control over how & when deployments happen

---

class: center, middle

# Plan a successful project

<i>Claim: Small projects (&lt;300h) are accurately estimatable in both time and cost.</i>

---

# Create project plan

Steps:
- Understand problem to be solved
- Develop solution concepts
- Formulate requirements 
- Estimate cost, reward
- Schedule implementation
- Analyse risks
- Feedback & repeat

> Target: Avoid as much work as possible while still solving the problem.

Reduced uncertainty as much as possible?  
Contract with customer & start!

---

# Understand business

Interview those who use & those who pay.  
Research industry, similar products & solutions (potentially of different industries).

Listen carefully:
- Repetitive "uncreative" work (=automatable)
- Proposed solutions
- Tedious tasks (=sucks)

You task is to solve their *problem*.

---

# Formulate requirements

Decompose concept to suitable abstraction level.  
Do not neglect non-functional requirements (performance, usability, ...).

YAGNI: You Ain't Gonna Need It.  
**Effort increases in O(exponential) due to complexity.** (*baseless claim alert*)

<img src="images/effort_increases_exponential.png" width="30%">
<img class="ml-2" src="images/effort_increase_with_new_feature.png" width="30%">


---

# Estimate requirements

Cannot predict the future, but **can estimate relative complexity**.  
Technology / mastery independent!

Tips:
- Use coarse-grained numbers (Fibonacci; 1 1 2 3 5 8 13 ...)
- With more unknowns, tend to increase complexity

Same for rewards.
Reward / Complexity = Payoff.

| Feature | Complexity | Reward | Payoff |
| :------ | ---------: | -----: | -----: |
| Sign-Up | 8 | 5 | 0.625 |
| Confirm E-Mail | | | |
| Login | | | |
| Password Recover | | | |
| Add Additional E-Mails | | | |


---

# Estimate requirements

Cannot predict the future, but **can estimate relative complexity**.  
Technology / mastery independent!

Tips:
- Use coarse-grained numbers (Fibonacci; 1 1 2 3 5 8 13 ...)
- With more unknowns, tend to increase complexity

Same for rewards.
Reward / Complexity = Payoff.


| Feature | Complexity | Reward | Payoff |
| :------ | ---------: | -----: | -----: |
| Sign-Up | 8 | 5 | 0.625 |
| Confirm E-Mail | 13 | 3 | 0.23 |
| Login | 5 | 8 | 1.6 |
| Password Recover | 21 | 5 | 0.24 |
| Add Additional E-Mails | 21 | 3 | 0.14 |


---

# Schedule implementation

Convert complexity into implementation effort.

| Feature | Complexity | Expected | Measured |
| :------ | ---------: | -------: | -------: |
| Sign-Up | 8 | - | 3h |
| Confirm E-Mail | 13 | 5h | ? |
| Login | 5 | 2h | ? |
| Password Recover | 21 | 8h | ? |
| Add Additional E-Mails | 21 | 8h | ? |

Multiplication factors:
- Technology (low for established, high for often changing)
- Complexity (increasing factor with more total features)
- Special requirements (translatable, performance, ...)

> 26h \* 1.1 (PHP only) \* 1.1 (translatable) =  31.5h  
> 26h \* 1.5 (PHP + JavaScript frontend) \* 1.1 (translatable) = 43h

---

# Schedule implementation

Combine functionality into **features** (=sensible packages).  
Resolve dependencies & reduce uncertainty.  

> Login 31.5h  
> Submit vote 23h  
> Electionsmanagement 10h  
> Candidatemanagement 16.5h  
> Vote count 8.5h

**Milestones** should be properly spaced & measurable.

> 1. Deployment of skeleton to server
> 2. Public voting area
> 3. Administration area
> 4. Feedback of trial election processed



---

# Risk analysis

For overall project & for each feature collect risks.

> Compromise of election data  
> Users reject tool due to security reasons  
> Server deployment too constrained

For each risk:
- Avoid: Eliminate the risk (avoid feature / use different solutions)
- Transfer: Shift the impact to a 3rd party (like your customer)
- Mitigate: Decrease the probability or impact (formulate "Plan B")

<span class="color-red">Do not skip risk analysis.</span>

---

# Contract

Formulate a contract.

Content:
- Vision ("Sales-pitch")
- Requirements on suitable abstraction level
- Schedule & Milestones
- Price & Payment agreements

Advanced:
- Recommend options based on payoff
- Maintenance for 10% - 20% yearly effort
- Legal: Intellectual Property, Bugfixing

Courses:
- [E-Business Recht](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=139975&semkez=2020W) (HS)
- [Contract Design](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=141671&semkez=2020W) (HS) 
- [Introduction into Negotiation](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=135075&semkez=2020S) (FS)

---

# Value of a good plan

**Up until now, an estimated 10 - 30% of total project effort has been spend!**  
Is it worth it?

Yes!
- Align expectations (cost, schedule, result)
- Decrease cost by not implementing lower-value features
- Decrease risk of project failure

It feels good:
- Make promises you can keep
- Customers / Users will be satisfied (and so will you!)
- You get done, get payed and move on.

*Hint: You can also sell this work as "requirements engineering" or "proof of concept"*.

---

class: center, middle

# Execute a successful project

<i>Core idea: No surprises.</i>

---

# Continuous tasks

*manage expectations*. Keep customer up to date with relevant developments. Incorporate doubts, recent developments and feedback 

> "The milestone is completed, and the next meeting can go along as planned."
> "Next big maintenance will be completed in 8 months."

*track progress*. Check if estimation matches measurements, and high-risk tasks proceed as expected. Take corrective action as early as sensible!

> Substantial increase in effort for feature: Systematic or one-time?  
> High-risk tasks: Solution in sight or stuck?

*long-term planning*. Resolve dependencies for future tasks.

> Hosting is ready in one week; delivery is due in two months. Hence can schedule meeting in four weeks to get feedback for final product.

---

# Rollout

Strategies:
- **Incremental**: Multiple releases, i.e. for each feature. *Additional effort due to shims for future features / release tasks.*
- **Big Bang**: Single release at the end. *No feedback till the end*.

**Incremental updates usually beats Big Bang**: The earlier issues are found / feedback is incorporated, the cheaper it is.

<hr/>

Changing `person.gender` from `Boolean` to `Text`:

| Start | End | 
| :------ | ---------: |
| Add field to table | Add field to table |
| | Find all usages of field |
| | Re-understand how and why it is used |
| | Find new solution & adapt code |
| | Re-test usage & all related features |

---

class: center, middle, inverted

<div class="pyro">
    <div class="before"></div>
    <div class="after"></div>
</div>

# Done!

---

class: center, middle

# Some closing notes

---

# About changes

Changes happen, deal with it.

Tools:
- Schedule features with uncertainty early.
- Explicitly acceptance test features / milestones.
- Clarify expectations (changes in cost / schedule).
- Define process for how & when changes are applied (and who pays for it).

Baseless claim: Ignoring change requests is as much a project failure as unapproved delays / cost overruns.

---

# About people

Important factor of project not in your control!

Get to know all involved persons:
- Motivation (agenda?)
- Way of working (early/late?, procrastination?)
- Experience (how have they performed in the past?)
- Personality (impulsive? diligent? analytical? risk-taking?)

Likely True Facts About People™:
- People are predictable
- People change very slowly (too slow for your project)
- People are good (or at least persuade themselves that they are)

Analysis approaches: [4 Colors](https://www.thecolourworks.com/insights-discovery-colour-types-guide/), [Big 5](https://www.simplypsychology.org/big-five-personality.html), [16 Personalities](https://www.16personalities.com/) (*(un)soundness alert*)

---

# Getting it done

*Getting it done* is hard! 
- Tedious tasks procrastinated till the end
- Some level of perfection needs to be reached
- Bugs right before / immediately after launch likely

Why most private projects never finish...

Tools:
- Assign buffer before release to get it done
- Keep days around release free
- Rollout incrementally to reduce severity of effects
- Set expectations of customer over risk of release

---

# Enterprise project management

Agile management:
- https://scrumguides.org/scrum-guide.html
- https://www.scaledagileframework.com/

Measure / estimate:
- Function points: Points depending on input / output complexity. [IBM, 1979](https://researcher.watson.ibm.com/researcher/view_group.php?id=7298)
- Use Case points: Translate components of use case diagram into sizes metrics. Adjust for complexity and technical factors. [Objective Systems, 1993](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.604.7842&rep=rep1&type=pdf)

[Project Management Course](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=149690&semkez=2021S&ansicht=KATALOGDATEN&lang=en):
- project management models (PMBOK, ...)
- agile management methods (SCRUM, Lean, Kanban, ...)

---

# Summary

**Favorable project environment**: Established vision & agreement over it. Sensible and stable business case. Sufficient experience, trust and capabilities.

**Project plan**: Solve business problem with as little work as possible. Use relative complexity / rewards to estimate. Schedule to reduce risk & uncertainty early. Analyse & control risk.

**Execution**: Manage expectations & progress. Rollout incrementally & incorporate feedback early. Deal humbly with changes and people. Get It Done.

---

class: center, middle

# Appendix

---

# About bad people

With some, you will not get along!

Powerful combination:
- Ignorance (don't care about everybody their work affects)
- Incompetence (don't have relevant experience)
- Arrogance (don't consider inputs)
- Rhetorics (can defend their shortcomings)

Options:
- Fight (usually a bad idea)
- Co-exist (sometimes not possible)
- Run (sometimes required)
    </textarea>
    <script src="js/remark.min.js" type="text/javascript"></script>
    <script src="js/remark.driver.js" type="text/javascript"></script>
  </body>
</html>
