# PDF

We take a deeper look into how a PDF file is structured, and learn about the pro / cons of the file format. As part of this talk, we will look at the PDF and TTF specification and a (unfinished) PDF write (a program that creates PDFs). At the end of this talk, you can create a minimal PDF from scratch in a text editor.
