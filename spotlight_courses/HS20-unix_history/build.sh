#!/bin/bash
echo "Building pdf..."
pandoc -t beamer --template template.tex --listings pres.md -o pres-part.pdf --pdf-engine pdflatex \
    && pandoc -t beamer --template template.tex --listings firstslide.md -o firstslide.pdf --pdf-engine pdflatex \
    && pdfunite firstslide.pdf pres-part.pdf pres.pdf \
    && rm firstslide.pdf pres-part.pdf \
    && echo "Build successful"
