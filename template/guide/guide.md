# Bash Quick Guide

Bash is an acronym for Bourne Again Shell. It is based on the Bourne shell and is mostly compatible with its features.

Shells are command interpreters. They are applications that provide users with the ability to give commands to their operating system interactively, or to execute batches of commands quickly. In no way are they required for the execution of programs; they are merely a layer between system function calls and the user.

Think of a shell as a way for you to speak to your system. Your system doesn't need it for most of its work, but it is an excellent interface between you and what your system can offer. It allows you to perform basic math, run basic tests and execute applications. More importantly, it allows you to combine these operations and connect applications to each other to perform complex and automated tasks.

Bash is not your operating system. It is not your window manager. It is not your terminal (but it often runs inside your terminal). It does not control your mouse or keyboard. It does not configure your system, activate your screen-saver, or open your files. It's important to understand that bash is only an interface for you to execute statements (using bash syntax), either at the interactive bash prompt or via bash scripts. The things that *actually happen* are usually caused by other programs.

This guide is based on [the bash guide in GreyCat's wiki](http://mywiki.wooledge.org/BashGuide) and aims to be more concise, while still being accurate. It was produced specifically for the Bash Workshop by [TheAlternative.ch](www.thealternative.ch). 

It is published under the [CC by-nc-sa 4.0 license](http://creativecommons.org/licenses/by-nc-sa/4.0/).

# Commands and Arguments

Bash reads commands from its input, which can be either a file or your terminal.
In general, each line is interpreted as a command followed by its arguments.

    ls
    touch file1 file2 file3
    ls -l
    rm file1 file2 file3

The first word is always the command that is executed. All
subsequent words are given to that command as argument.

Note that *options*, such as the `-l` option in the example above, are not treated specially by bash. They are arguments like any other. It is up to the program (`ls` in the above case) to treat it as an option.

Words are delimited by whitespace (spaces or tabs). It does not matter how many spaces there are between two words. For example, try

    echo Hello            World

The process of splitting a line of text into words is called *word splitting*. It is vital to be aware of it, especially when you come across expansions later on.

## Preventing Word Splitting

Sometimes, you will want to pass arguments to commands that contain whitespace. To do so, you can use quotes:

    touch "Filename with spaces"

This command creates a single file named *Filename with spaces*. The text within double quotes is protected from word splitting and hence treated as a single word.

Note that you can also use single quotes:

    touch 'Another filename with spaces'

There is, however, an important difference between the two:

* Double quotes prevent **word splitting**
* Single quotes prevent **word splitting and expansion**

When you use single quotes, the quoted text will never be changed by bash. With double quotes, expansion will still happen. This doesn't make a difference in the above example, but as soon as you e.g. use variables, it becomes important.

In general, it is considered good practice to use single quotes whenever possible, and double quotes only when expansion is desired. In that sense, the last example above can be considered "more correct".

## The Importance of Spaces

Bash contains various keywords and built-ins that aren't immediately recognizable as commands, such as the new test command:

    [[ -f file ]]

The above code tests whether a file named "file" exists in the current directory. Just like every line of bash code, it consists of a command followed by its arguments. Here, the command is `[[`, while the arguments are `-f`, `file` and `]]`.

Many programmers of other languages would write the above command like so:

    [[-f file]]

This, though, is wrong: Bash will look for a command named `[[-f`, which doesn't exist, and issue an error message. This kind of mistake is very common for beginners. It is advisable to always use spaces after any kind of brackets in bash, even though there are cases where they are not necessary.

# Variables and Parameters

Variables and parameters can be used to store strings and retrieve them later. *Variables* are the ones you create yourself, while *special parameters* are pre-set by bash. *Parameters* actually refers to both, but is often used synonymously to special parameters.

To store a string in a variable, we use the *assignment syntax*:

    varname=vardata

This sets the variable `varname` to contain the string `vardata`.

Note that you cannot use spaces around the `=` sign. With the spaces, bash would assume `varname` to be a command and then pass `=` and `vardata` as arguments. 

To access the string that is now stored in the variable `varname`, we have to use *parameter expansion*. This is the most common kind of expansion: A variable is replaced with its content.

If you want to print the variable's value, you can type

    echo $varname

The `$` indicates that you want to use expansion on `varname`, meaning it is replaced by its content. Note that expansion happens before the command is run. Here's what happens step-by-step:

* Bash uses variable expansion, changing `echo $varname` to `echo vardata`
* Then, bash runs `echo` with `vardata` as its parameter.

The most important thing here is that **variable expansion happens before wordsplitting**. That means, if you have defined a variable like this:

    myfile='bad song.mp3'

and then run the command

    rm $myfile

bash will expand this to

    rm bad song.mp3

Only now, word splitting occurs, and bash will call `rm` with two arguments: `bad` and `song.mp3`. If you now had a file called `song.mp3` in your current directory, that one would be deleted instead.

To prevent this from happening, you can use double quotes:

    rm "$myfile"

This will be expanded to

    rm "bad song.mp3"

which is what we want. In this case, you have to use double quotes, as single quotes would prevent expansion from happening altogether.

Not quoting variable and parameter expansions is a very common mistake even among advanced bash programmers. It can cause bugs that are hard to find and can be very dangerous. **Always quote your variable expansions.**

You can also use variable expansions inside the variable assignment itself. Consider this example:

    myvariable='blah'
    myvariable="$myvariable blah"
    echo "$myvariable"

What will the output of this script be?

First, the variable `myvariable` will get the value `blah`. Then, `myvariable` is assigned to again, which overwrites its former content. The assignment contains a variable expansion, `"$myvariable blah"`. This is expanded to `"blah blah"`, and that is going to be the new value of `myvariable`. So the last command is expanded to `echo "blah blah"`, and the output of the script is `blah blah`.

## Special Parameters

*Special parameters* are variables that are set by bash itself. Most of those variables can't be written to and they contain useful information.

Parameter Name | Usage | Description
-----------|-----------|---------------------------------------------------------------------
`0`              | `"$0"`      | Contains the name of the current script
`1` `2` `3` etc. | `"$1"` etc. | Contains the arguments that were passed to the current script. The number indicates the position of that argument (first, second...). These parameters are also called positional parameters.
`*`              |  `"$*"`      | Contains all the positional parameters. Double quoted, it expands to a single word containing them all.
`@`              | `"$@"`      | Contains all the positional parameters. Double quoted, it expands to **several words, where each word is one parameter**. This is special syntax, it behaves differently from "normal" expansion in quotes. It retains the arguments exactly as they were passed to the script.
`#`              | `"$#"`      | Contains the number of parameters that were passed to the script
`?`              | `"$?"`      | Contains the exit code of the last executed command

# Globbing

*Globs* are an important bash concept--mostly for their incredible convenience. They are patterns that can be used to match filenames or other strings.

Globs are composed of normal characters and metacharacters. Metacharacters are characters that have a special meaning. These are the metacharacters that can be used in globs: 

* `*`: Matches any string, including the empty string (i.e. nothing)
* `?`: Matches any single character
* `[...]`: Matches any one of the characters enclosed in the brackets

Bash sees the glob, for example `a*`. It expands this glob, by looking in the current directory and matching it against all files there. Any filenames that match the glob are gathered up and sorted, and then the list of filenames is used in place of the glob. So if you have three files `a`, `b` and `albert` in the current directory, the glob is expanded to `a albert`.

A glob always has to match the entire filename. That means `a*` will match `at` but not `bat`. 

Note that globbing is special in that it happens *after word splitting*. This means you never need to worry about spaces in filenames when you use globbing, and quoting globs is not necessary. In fact, quotes will prevent globbing from happening.

* `/home/username/pictures/my picture001.jpg` 
* `/home/username/pictures/my picture002.jpg`
    
and this is exactly what we wanted.

So, remember: Quotes don't need to be at the beginning or end of an argument, and if you use several kinds of expansion together, you can add quotes in the middle as required.

## Expansion Order

All the kinds of expansion happen in a certain order. The order is as follows:

* Brace expansion
* Tilde expansion
* Parameter and variable expansion
* Command substitution
* Arithmetic expansion
* Word splitting
* Globbing

# Useful Commands

This chapter provides an overview of useful commands that you can use in your scripts. It is nowhere near complete, and serves only to provide a brief overview. If you want to know more about a specific command, you should read its manpage.

**grep**

`grep` can be used to search for a string within a file, or within the output of a command.

    # searches logfile.txt for lines containing the word error
    grep 'error' logfile.txt  

    # searches the directory 'folder' for files 
    # containing the word 'analysis'
    grep 'analysis' folder/   

    # searches the output of 'xrandr' for lines that say 'connected'. 
    # only matches whole words, so 'disconnected' will not match.
    xrandr | grep -w 'connected' 

`grep` returns 0 if it finds something, and returns an error if it doesn't. This makes it useful for conditionals.

**sed**

`sed` can be used to edit text "on the fly". It uses its own scripting language to describe modifications to be made to the text, which makes it extremely powerful. Here, we provide examples for the most common usages of sed:

    # replaces the first occurrence of 'find' in every line by 'replace'
    sed 's/find/replace' inputfile 

    # replaces every occurrence of 'find' in every line by 'replace'
    sed 's/find/replace/g' inputfile 

    # deletes the first occurrence of 'find' in every line
    sed 's/find//' inputfile 

    # deletes every occurrence of 'find' in every line
    sed 's/find//g' inputfile 

    # displays only the 12th line
    sed '12q;d' inputfile 

`sed` is often used in combination with pipes to format the output or get rid of unwanted characters.

**curl and wget**

`curl` and `wget` are two commands that can be used to access websites or other content from the web. The difference is that `wget` will simply download the content to a file, while `curl` will output it to the console.

    curl http://www.thealternative.ch
    wget http://files.project21.ch/LinuxDays-Public/16FS-install-guide.pdf

**xrandr**

`xrandr` can be used to manipulate your video outputs, i.e. enabling and disabling monitors or setting their screen resolution and orientation.

    # list all available outputs and their status info
    xrandr  

    # enables output HDMI-1
    xrandr --output HDMI-1 --auto 

    # puts output HDMI-1 to the left of output LVDS-1
    xrandr --output HDMI-1 --left-of LVDS-1 

    # disables output LVDS-1
    xrandr --output LVDS-1 --off 

**ImageMagick (convert)**

The `convert` command makes it possible to do image processing from the commandline.

    # scale fullHDWallpaper.jpg to specified resolution
    convert fullHDWallpaper.jpg -scale 3200x1800 evenBiggerWallpaper.jpg 

    # "automagically" adjusts the gamma level of somePicture.jpg
    convert somePicture.jpg -auto-gamma someOtherPicture.jpg 

    # transform image to black and white
    convert colorfulPicture.jpg -monochrome blackWhitePicture.jpg 

It is extremely powerful and has lots of options. A good resource is the [official website](http://www.imagemagick.org). It also provides examples for most options.

**notify-send**

`notify-send` can be used to display a desktop notification with some custom text:

    notify-send 'Battery warning' 'Your battery level is below 10%'

The first argument is the notification's title, the second is its description.

`notify-send` requires a *notification daemon* to be running, else it won't
work. Most desktop environments come with a notification daemon set up and
running. If you can't see your notifications, it might be that you don't have
such a daemon.

**find**

`find` can be used to find files in a directory structure. 
    
    # finds all files in the current directory and all subdirectories that end
    # in .png
    find -name '*.png' 

    # finds all files ending in .tmp and removes them. {} is replaced by the
    # file's name when executing the command.
    # Note that we don't use globbing here, but instead pass the * to find.
    # find will then interpret the * as a wildcard.
    find -name '*.tmp' -exec rm '{}'

    # finds all files in the directory 'files' and prints their size and path
    find 'files/' -printf '%s %p\n' 

`find` has many options that allow you to perform arbitrary actions on the files it found or pretty-print the output.

**sort**

`sort` sorts lines of text files, or lines it reads from *stdin*. 

    sort listOfNames.txt # sorts all lines in listOfNames.txt alphabetically

**head and tail**

`head` and `tail` can be used to show the beginning or the end of a long stream of text, respectively.

    # display the last few lines of the dmesg log
    dmesg | tail 

    # display only the first few lines of a very long text file
    head verylongtext.txt 

**jq**

`jq` is a very simple command-line json parser. It can read data in json format and return specific values.

Its most important options are `-e` and `-a`. `-e` extracts the value of a given key from a json array or object:

    # Find the object with key "title" from a json object stored in the file "json"
    jq '.title' < 'json'

The `-a` option maps all remaining options across the currently selected element. It has to be combined with other options, for example the `-e` option. 

    # Find the names of all elements stored in the json object in file "json"
    jq '[].name' < 'json'


**shuf**

`shuf` randomly permutates the lines of its input, effectively *shuffling* them.

    # Shuffle the lines of file "playlist"
    shuf 'playlist'

    # Get a random line from file "quotes"
    shuf -n 1 'quotes'

**tee**

`tee` takes input from *stdin* and writes it to a file:

    sudo zypper up | tee 'updatelog'

Note that this is equivalent to

    sudo zypper up > 'updatelog'

`tee` is useful when you want to write to a file that requires root access. Then you can do the following:

    echo 'some configuration' | sudo tee '/etc/systemconfig'

A normal redirection wouldn't work in this case, as that would open the file as a normal user instead of root. 
**Please be careful when modifying system files.**

**sleep**

`sleep` pauses the execution of your script for a number of seconds. It basically takes a number as an argument, then does nothing for that number of seconds, and then returns. It can be useful if you want to make your script do something in a loop, but want to throttle the speed a little.

    # prints "Get back to work" once per second. 
    # Note that 'true' is a command that always returns 0.
    while true
    do
        echo 'Get back to work!'
        sleep 1
    done

**mplayer or mpv**

`mplayer` and `mpv` are media players that can be used from the console. `mpv` is based on `mplayer` and a lot more modern, but they can do essentially the same.

    # play file "music.mp3"
    mplayer 'music.mp3'
    mpv 'music.mp3'

    # play file "alarm.mp3" in an infinite loop
    mplayer -loop 0 'alarm.mp3'
    mplayer --loop=inf 'alarm.mp3'

    # play a youtube video
    # this only works with mpv and requires youtube-dl to be installed
    mpv 'https://www.youtube.com/watch?v=lAIGb1lfpBw'

**xdotool**

`xdotool` can simulate keypresses. With this command, you can write macros that perform actions for you. It allows you to write scripts that interact with GUI programs.

    # press ctrl+s (in order to save a document)
    xdotool key 'Control+s'

    # type "hello"
    xdotool type 'hello'

