#!/bin/bash
echo "Building pdf..."
pandoc guide.md --listings --toc -N -V links-as-notes -H listings-setup.tex -o guide.pdf \
    && echo "Build successful"

echo "Building html..."
pandoc --toc -N --template template.html --highlight-style tango guide.md | sed 's/<code>/<code class="prettyprint">/g' > bashguide.html \
    && echo "Build successful"
