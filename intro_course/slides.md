---
author:
- Fadri Lardon
title: Introduction to Linux
---

# Linux, what's that?

## What even is Linux?

### 

\bigtext{So you want to use Linux?}

###

\hskip 3em \includegraphics[height=0.9\textheight]{img/cardreader.jpg}

###

\hskip 3em \includegraphics[height=0.9\textheight]{img/ticketautomat.jpg}

### 

\hskip 3em \includegraphics[height=0.9\textheight]{img/android_logo.jpg}

###

\hskip 3em \includegraphics[height=0.9\textheight]{img/internet.jpg}

### Linux, what does it mean?

* A *Linux system* is a system that uses the Linux *Kernel*.

\vskip 1em

\hskip 4em \includegraphics[width=0.8\textwidth]{img/linux_kernel.pdf}

### 

\bigtext{Linux}

\vskip 1em

\hskip 4em \includegraphics[width=0.8\textwidth]{img/puzzlepiece.pdf}

### 

\bigtext{GNU/Linux}

\vskip 1em

\hskip 4em \includegraphics[width=0.8\textwidth]{img/linux_kernel_blank.pdf}

# The pieces of a Linux system

## It's like LEGO

### 

\hskip 3em \includegraphics[height=0.9\textheight]{img/desktop-computer.jpg}

### 

\includegraphics[width=\textwidth]{img/pc-components.jpg}

### System components

* Kernel \soft{the core of your OS that manages all other programs and your devices}

* Bootloader \soft{a tiny program that runs first when you power your PC and takes care of starting up the kernel}

* Init system \soft{a program that takes care of starting and stopping background tasks}

* Package manager \soft{a program that allows you to install other programs}

* Graphics framework \soft{a basic software that allows other programs to draw stuff on the screen}

* Shell \soft{the program that powers the console}

### System components you care about

* Desktop Environment and/or Window Manager
* Editor
* Browser
* Mail client

### My system

\hskip 6em \includegraphics[height=0.8\textheight]{img/my_system.pdf}

### Standard Ubuntu system

\hskip 6em \includegraphics[height=0.8\textheight]{img/ubuntu_system.pdf}

# Linux Concepts

##

### <!-- WMP vs VLC -->

\includegraphics[width=0.4\textwidth]{img/wmp.jpg}
\raisebox{4em}{\bigtext{vs.}}
\hskip 1em
\includegraphics[width=0.4\textwidth]{img/vlc.pdf}

### <!-- WMP vs VLC -->

\includegraphics[width=0.4\textwidth]{img/wmp.jpg}
\raisebox{4em}{\bigtext{vs.}}
\hskip 1em
\includegraphics[width=0.4\textwidth]{img/vlcdeps.pdf}

<!-- pacvis -->
### {.plain} 

\includegraphics[width=\textwidth]{img/pacvis.png}

### The Unix Philosophy

\bigtext{Do one thing, and do it right}

### 

\includegraphics[width=\textwidth]{img/SUSE-community-photo.jpg}

<!-- pacvis -->
### {.plain} 

\includegraphics[width=\textwidth]{img/pacvis.png}

### Package Managers

* Your best friend when it comes to dependency management
* Installs and removes software
* Updates all your programs with one click

### Package Managers

\hskip 7em \includegraphics[height=0.85\textheight]{img/package-manager-blank.pdf}

### Refresh

\hskip 7em \includegraphics[height=0.85\textheight]{img/package-manager-refresh.pdf}

### Upgrade or Install

\hskip 7em \includegraphics[height=0.85\textheight]{img/package-manager-upgrade.pdf}

# Distributions

## 

### 

\hskip 6em \includegraphics[height=0.8\textheight]{img/my_system.pdf}

### <!-- Choose all the things? -->

\bigtext{But how do I choose?}

### Fear not!

\hskip 2em \includegraphics[height=0.8\textheight]{img/logos.png}

### My system (Arch Linux)

\hskip 6em \includegraphics[height=0.8\textheight]{img/my_system_arch.pdf}

### Standard Ubuntu system

\hskip 6em \includegraphics[height=0.8\textheight]{img/ubuntu_system_orange.pdf}

### Distributions

\hskip 9em \includegraphics[height=0.8\textheight]{img/distro-timeline.jpg}

### Ubuntu (and forks)
\begin{minipage}{0.6\textwidth} 
  \begin{itemize}
    \item Simplicity and ease of use
    \item Provide a complete package that "just works"
    \item Polished experience
  \end{itemize}
\end{minipage}\begin{minipage}{0.4\textwidth}
  \includegraphics[width=\textwidth]{img/ubuntu_logo.png}
\end{minipage}

### OpenSUSE
\begin{minipage}{0.6\textwidth} 
  \begin{itemize}
    \item Customisability
    \item Provide a complete package that you can adapt to suit your needs
    \item Focus on free software
  \end{itemize}
\end{minipage}\begin{minipage}{0.4\textwidth}
  \includegraphics[width=\textwidth]{img/opensuse_logo.png}
\end{minipage}

### Fedora
\begin{minipage}{0.6\textwidth} 
  \begin{itemize}
    \item Innovation and being on the leading edge
    \item Provide a complete package with all the most recent software
    \item Polished experience with just free software
  \end{itemize}
\end{minipage}\begin{minipage}{0.4\textwidth}
  \hskip 1em \includegraphics[width=0.9\textwidth]{img/fedora_logo.png}
\end{minipage}

### Outlook: Arch Linux
\begin{minipage}{0.6\textwidth} 
  \begin{itemize}
    \item Keeping to the bare minimum
    \item Provide just the basics, let the user choose all components
    \item Make a system that is truly yours
  \end{itemize}
\end{minipage}\begin{minipage}{0.4\textwidth}
  \includegraphics[width=\textwidth]{img/arch_logo.png}
\end{minipage}

### Choose Your Distribution

* They may have different puzzle pieces but there's also a big overlap
* Pick whichever sounds good to you
* If you want some help choosing, don't hesitate to ask us!

# Desktop environments

## DEs DEs DEs

###

\bigtext{Desktop Environments}

### Puzzle time

\hskip 6em \includegraphics[height=0.8\textheight]{img/gnome.pdf}

### KDE 

\hskip 8em \includegraphics[height=0.8\textheight]{img/kde.png}

### KDE 

* Rich in features
* Very customisable
* Similar to Windows UI

### KDE

\hskip 8em \includegraphics[height=0.8\textheight]{img/plasma_screenshot.png}

### Gnome 

\hskip 10em \includegraphics[height=0.8\textheight]{img/gnome3.png}

### Gnome 

* Well put together, one experience
* Looks great
* Extendable through extensions but otherwise not very customisable

### Gnome

\hskip 10em \includegraphics[height=0.8\textheight]{img/gnome_screenshot.png}

### XFCE

\hskip 7.5em \includegraphics[height=0.8\textheight]{img/Xfce_logo.pdf}

### XFCE

* Small and fast, good for older PCs
* Customisable
* Similar to Windows UI

### XFCE 

\hskip 10em \includegraphics[height=0.8\textheight]{img/Xfce_screenshot.png}

### Cinnamon

\hskip 10em \includegraphics[height=0.8\textheight]{img/cinnamon-logo.png}

### Cinnamon

* dependable
* made with GUI in mind
* easy to get used to if you're coming from windows

### Cinnamon

\hskip 10em \includegraphics[height=0.8\textheight]{img/cinnamon_screenshot.jpg}

# Switching to Linux

## what to keep in mind

### Tux won't break your existing OS

\hskip 6em \includegraphics[height=0.8\textheight]{img/tux-break-os.jpg}

###

\includegraphics[height=0.9\textheight]{img/multiboot.jpg}

### Software

* Most popular programs have an alternative for Linux
* Often they even run natively!

### Install now choose later

* You can switch out most components after installing.
* especially your desktop environment.
* modularity means choice

### 

\bigtext{But now... why is Linux so cool?}

### {.plain}

\includegraphics[height=\textheight]{img/xfce-rice1.png}

### {.plain}

\includegraphics[width=\textwidth]{img/xfce-rice2.png}

### {.plain}

\includegraphics[width=\textwidth]{img/hyprland_rice.png}

### Very Configurable Programs

If you don't like the way something works, you can just change it!

### Error messages

\hskip 8em \includegraphics[height=0.8\textheight]{img/windows-network-troubleshooting.png}

### Error messages

\hskip 8em \includegraphics[height=0.8\textheight]{img/cnf.png}

### Community support instead of expensive hotlines

\hskip 8em \includegraphics[width=\textwidth]{img/archforum.png}

### Do updates with ease

\includegraphics[width=\textwidth]{img/windowsupdate.png}

### Use powerful tools

* With comparatively little effort, you can automate tedious tasks

<!-- 
### Use powerful tools

* Update and then automatically shut down your PC
* Assign a keyboard shortcut to start playing your favourite webradio
    * or change your wallpaper
    * or reset your screen brightness
* Convert all image files in a directory to a different format with a single command
* Automatically sort your Downloads folder whenever you download a new file
-->

<!-- -->

* *Want to know more?* Visit our Linux Toolbox course!

### 

\bigtext{Don't worry too much, it's fun!}

# Epilogue

##

### Next steps

* Ask us any questions
* Come to our other talks
* Install Linux! \soft{please back up your data first}
* Join our community

<!-- -->

* Leave some feedback \
  \soft{https://feedback.thealternative.ch/}

### Other Courses

* Linux Toolbox: Friday, 16.04. 17:15-19:00
* Git: Monday, 19.04. 17:15-19:00
* How does Zoom Store Recordings: Friday, 23.04. 17:15-18:00
* A World without Intel: Wednesday, 28.04 17:15-19:00
* IT Project Management: Thursday, 29.04. 17:15-18:00
* PDF = PestesDateiFormat: Friday, 07.05. 17:15-18:00
\vskip 1em \centering Registration at \soft{thealternative.ch}

### Course material
* These slides: \
  \soft{https://gitlab.ethz.ch/thealternative/courses/tree/master/intro\_course}

<!-- -->

* \soft{Theme by} Christian Horea, [CC BY](https://creativecommons.org/licenses/by/4.0/)
