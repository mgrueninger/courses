# Bash Course by Alexander Schoch, Nicolas König

Folder contains the tex files required to self compile the presentation.
A pdf version is also included. 

Current Version : April 17, 2023 

## Topics 
- Basic terminal commands (cd, cp, ls, ...)
- Piping
- Basic scripting
