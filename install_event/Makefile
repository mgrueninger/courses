html_out := build/installguide.html
pdf_out  := build/installguide.pdf
full_tex := full-guide.tex
tmp_tex  := $(pdf_out:.pdf=.tex)
git_tags := build/GIT_TAG

PANDOC_FLAGS_HTML=\
	--table-of-contents \
	--number-sections \
	--highlight-style tango \
	--metadata-file metadata.yaml

PANDOC_FLAGS_TEX=--from markdown+link_attributes

TFLAGS=\
	-pdf \
	-output-directory=build

.PHONY: clean distclean default

default: builddir html pdf

html: *.md
	pandoc $(PANDOC_FLAGS_HTML) $? -o $(html_out)

pdf: $(pdf_out)

$(pdf_out): $(tmp_tex) $(git_tags)
	latexmk $(TFLAGS) $(full_tex)

$(tmp_tex): *.md
	pandoc $(PANDOC_FLAGS_TEX) $? -o $@

builddir:
	@[ -d build ] || mkdir build

$(git_tags):
	printf "r%s.%s" "$$(git rev-list --count HEAD)" "$$(git rev-parse --short HEAD)" > $@

clean:
	$(RM) -rf $(tmp_tex) $(git_tags) build/*

distclean: clean
	$(RM) $(html_out) $(pdf_out)
