\mysubsection*{Bootloader Hack}
\label{subsec:boot-hack}

Some EFI systems do not implement the EFI standard properly, which leads to
problems.

### Symptoms

* No matter what boot order you set with `efibootmgr`, Windows still boots.
* Settings you change with `efibootmgr` keep disappearing on reboot.
* Sometimes, it's possible to get to GRUB by "using a device" to boot. A boot
  device named "opensuse", "ubuntu" or "grub" might show up.

### How-To (Non-Mac Computers)

-    Try to boot the actual distro that has been installed by "using a device"
     to boot. Do so by holding shift while clicking on "reboot" in Windows.
-    If that doesn't work, boot a live system and chroot into the installed
     system. If you don't know how to do that, ask the Patrol for the manual.
-    Once booted or chrooted into the Linux system, become root (sudo su) and
     go to `/boot/efi/EFI/Microsoft/Boot` and locate a file named
     `bootmgfw.efi`. Rename it to `bootmgfw-original.efi`.
-    Go to `/boot/efi/EFI/grub2` (sometimes also just `grub`) and locate the
     file `grubx64.efi`. Copy it over to `/boot/efi/EFI/Microsoft/Boot/`. If a
     file called `shimx64.efi` exists, copy that one over as well.
-    Find the file `grub.cfg` in `/boot/efi/EFI/grub2` and copy it over to
     `/boot/efi/EFI/Microsoft/Boot/`.
-    Go back to `/boot/efi/EFI/Microsoft/Boot/`. If `shimx64.efi` exists,
     rename it to `bootmgfw.efi`. If it does not exist, rename `grubx64.efi` to
     `bootmgfw.efi`.
-    Now go to `/boot/grub/`, or `/boot/opensuse/` (the exact folder path may
     vary). Find the file `grub.cfg` and open it. Find the `menuentry` block
     for Windows (usually called "Windows Bootloader (on /dev/sdx)" or 
     similar). Copy the entire block.
-    Uninstall the package `os-prober`.
-    Now go to /etc/grub.d. If a file with `os-prober` in its name exists,
     delete it.
-    Find the file `40-custom.cfg`. If it doesn't exist, create it. Paste the
     menuentry block you copied earlier in this file.
-    In the text you just pasted, look for `bootmgfw.efi` and change it to
     `bootmgfw-original.efi`.
-    Save the file.
-    Run `grub-mkconfig -o /boot/grub/grub.cfg`. Make sure the file path
     matches the path where you originally found the `grub.cfg` you copied the
     menuentry from.
-    Reboot and verify that grub now loads properly. Also test whether Windows
     boots!

For reference, ask Aline, Max or Jan.

\cleartooddpage
