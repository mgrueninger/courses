\mysubsection*{Surface Pro 4 \& Surface Book}
\label{subsec:sflake-sp4b}

These little things need some special treatment to get the touchscreen
to work. We only support them under OpenSUSE, so this manual assumes
OpenSUSE is installed.

After the installation, add the following repository:

`http://download.opensuse.org/repositories/home:/maxf:/LD/openSUSE\_Leap\_42.2/`

From there, install kernel-default-ipts and kernel-firmware-ipts (for
intel precise touch support). **Uninstall the default kernel**.
Unfortunately, the SP4 drivers are only available for kernel 4.4, so you
are effectively downgrading the Linux kernel. If you leave the default
kernel installed, grub will boot it since it is newer.

After that, you need to copy some hardware descriptor files from the
Windows partition:

-   iaPreciseTouchDescriptor.bin

-   SurfaceTouchServicingDescriptorMSHW0078.bin

-   SurfaceTouchServicingKernelSKLMSHW0078.bin

-   SurfaceTouchServicingSFTConfigMSHW0078.bin

These files are located in:

`\\Windows\\inf\\PreciseTouch\\Intel`

Note that the name might vary slightly. Specifically, there might be different versions of the latter three files, which different numbers at the end (76, 78, 79...). If that is the case, use the files with the highest number.

Kernel will look for these user space binaries in /itouch folder under
specific names (same order as above):

-   /itouch/integ\_descriptor.bin

-   /itouch/vendor\_descriptor.bin

-   /itouch/vendor\_kernel\_skl.bin

-   /itouch/integ\_sft\_cfg\_skl.bin

After that, reboot and verify the touchscreen works.

For further reference, ask Max or Aline or see

https://github.com/ipts-linux-org/ipts-linux/wiki\#user-space-components

###Touchscreen Inverted

On the Surface Book, we have observed that the touchscreen is inverted after the above steps (the y axis is mirrored, so when you type in the bottom left corner, the reaction happens in the top left corner). To fix this, we use xinput to set the coordinate transformation matrix.

Note that only finger touch is affected, touch with the Surface Pen is correct.

* Install xinput (sometimes named xorg-xinput).
* Run `xinput` and look for a touch device that might be the touchscreen. There should be two devices, one named something like `0000:0000` and the other `0000:0000 Pen`. You want the one without the `Pen` suffix.
* Run `xinput set-prop "Device Name" "Coordinate Transformation Matrix" 1 0 0 0 -1 1 0 0 1`, where "Device Name" is the name of the touchscreen you found previously.
* Test whether the touchscreen behaves properly now.

Note that this solution is temporary. We have not yet tested any ways to
make it permanent. You could try to put the above `xinput
set-prop` command in `~/.xinitrc` or some other auto-start script. The proper solution would be to create a file for the touchscreen in `/etc/X11/xorg.conf.d/` and set the `TransformationMatrix` option. If you find a working solution, please tell the Patrol.

\pagebreak
