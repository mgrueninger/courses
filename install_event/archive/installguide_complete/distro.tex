\begin{center}
\textbf{\Large What distribution should I pick?}
\end{center}\vskip1em

Linux is an operating system of choices, which can be overwhelming for beginners.

\textbf{First, you need to pick your Linux distribution.} This page gives you an overview of the various distributions and their advantages.

In order to help you with the choice of distribution, we have two resources ready for you:
\begin{itemize}
	\item Below you find a quick overview of some distributions we picked out for you.
	\item To get a hands-on experience, you can try some distributions on our demo laptops in the auxiliary room.
\end{itemize}

Note that at this point, we have prepared guides for \textbf{Ubuntu and OpenSUSE}. If you want anything
other than that, we will gladly help you find appropriate guides online.

If you want more information or advice, you can ask a helper. Note, though, that every helper might have a different opinion on the distributions.

\begin{description}
	\item[Ubuntu]
		is a beginner-friendly, popular distro, which requires nearly no
		configuration. It comes with the Unity desktop, which OS X users may
		find familiar. There are also official Ubuntu spinoffs with other
		desktop environments (DEs). Due to Ubuntu's popularity, it's easy to
		find help on the internet when you have a problem, which makes it a
		good choice for newcomers. Unlike most other distributions, which are
		maintained by independent communities, Ubuntu is developed by
		Canonical, a software company. Be warned that many have concerns with
		Canonicals controversial stance on the ideals of open source software,
		the rest of the FOSS community, and privacy practices.\\
        \textbf{Versions:} Ubuntu has two different release versions, the current version,
        and the LTS (Long term support) version. LTS is usually supported for
        four years, and is supposed to be as stable as possible. Because of this,
        it often has older versions of software. Choose LTS if you are after extreme
        stability or if you have
        specific requirements. If you want a more modern user experience, the current
        release is the better option.\\
		\suggestions{Beginners}
        \de{Xfce (with Xubuntu), KDE 5 (with Kubuntu), Gnome}

	\item[OpenSUSE]
		is a unique distro that targets both experienced and new users. It offers a
		great compromise between configurability and ease of use, making all
		its features accessible without ``dumbing down'' the interface. It is known
		for its YaST system configuration tool, which you can use to do tweaks
		you would otherwise need the terminal for. This distribution is
		maintained by the openSUSE community, but is based on the commercial
		Suse Linux Enterprise Edition. This makes it a stable, well
		rounded distro. We distinguish between OpenSuse Tumbleweed (rolling release,
		bleeding-edge software) and Leap (versionned as Ubuntu, more stable than Tumbleweed).
		\\
		\suggestions{For users wanting a sophisticated and user-friendly system.}
		\de{KDE, Xfce, Gnome, LXDE}
		\note{OpenSUSE Leap is only for modern 64-bit computers. Additionally,
		the OpenSuse installer will not work with newer Lenovo devices (released in 2017
		or later), such as the T470 Thinkpad and the Yoga 370.
		If you have one of these devices, you will need to install a different Distro.}

	\item[CentOS]
		is the free brother of Red Hat Enterprise Linux (RHEL). RHEL is built for professional
		purposes and comes with a support contract that companies pay for. If you'd like to take
		advantage of the technologies of Red Hat Enterprise, but as an individual, without paying
		for it, then CentOS is the choice for you. It is basically RHEL without the commercial
		aspect. This makes it a very stable and mature distribution, backed by Red Hat.\\
		\suggestions{experienced users and developers}
		\de{Gnome 3 (default); KDE Plasma}

	\item[Fedora]
		is community maintained, yet corporately backed by Red Hat; similarly to
		openSUSE. The difference to CentOS is that Fedora is the ``Playground'' for
		Red Hat. Fedora is normally the first distro to introduce new features,
		which are eventually adopted by other distros. Many Linux developers
		(including Linus Torvalds himself) use Fedora. It normally comes with
		the GNOME 3 desktop, but also offers "spins" with different DEs. The
		default software repositories (repos) only include free software, but you
		can enable the RPMFusion repos which contain the non-free software.
		It is the distribution installed on most ETH desktop computers.\\
		\suggestions{experienced users and developers}
		\de{Gnome 3 (default); KDE Plasma, Xfce, LXDE, MATE-Compiz, Cinnamon}

 	\newpage

	\item[Linux Mint]
		is a Ubuntu-based distro, which focuses on preserving
		the traditional desktop (like Windows 7).
        Its community is a lot smaller than for example Ubuntu's, which
		makes it hard to find help when problems occur. Also, it is maintained
		by a rather small group of developers, which means it usually takes a
		while for them to react on user feedback.\\
		\suggestions{Beginners}
		\de{MATE, Cinnamon}

	\item[Debian]
		is a universal distro, which has been going strong for over 20 years, and is
		the most forked distro. Debian is rather lightweight, requiring you to pick your own
		components, but configures them automatically to a degree.
		It strictly follows its principles of free software, although it includes
		non-free software on non-free repositories. It has 3 branches: stable,
		unstable, and testing. Debian has a reputation
		of being very conservative about implementing new features in their stable release.\\
		\suggestions{more experienced users who already know what they want}
		\de{Gnome, KDE, Xfce, LXDE, MATE}
		
	\item[Arch Linux]
		is a distribution that focuses on simplicity and bleeding-edge software. It is rolling
		release, meaning that software is shipped as soon as it is released and Arch does not
		have version numbers (such as ``Ubuntu 16.04'' or ``Fedora 27''). Unlike
		the other distros presented above, Arch has no default Desktop Environment, browser,
		mail client etc: it leaves the choice up to you. This makes Arch Linux a distribution
		for more advanced Linux users. It is \textit{possible} for particularly motivated
		enthusiasts to make Arch their first Linux experience, but the learning curve is extremely
		steep. We do not recommend installing Arch Linux in the Install Events. If you choose
		Arch, better ask someone if they are willing to set down with you personally for a few
		hours and help you getting started.
\end{description}

\vspace{15pt}

In the following graph, we try to give an overview of the distributions presented above. Keep in mind that this representation is not objective but reflects the opinion of the authors. There are no exact measures concerning novelty and user-friendliness.

We consider distros on the left side easier to dive into for people who never used Linux. In principle, it is possible to start with any distro, but the learning curve is steeper for distros on the right in the diagram.

The higher a distro appears in the diagram, the more ``bleeding-edge'' it is. When a new version of a software comes out, it will be included in the bleeding-edge distros first. This comes at the price of having lower reliability since many bugs are yet undetected. On the other hand, distros on the lower part are more conservative, but more stable.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.6]{img/distros_property_graph.png}
\end{figure}
